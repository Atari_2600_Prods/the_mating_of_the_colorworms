
.segment "ZEROPAGE"

value:
   .word $0000
delta:
   .word $0000

RODATA_SEGMENT

.if NTSC
.else
highs:
   .byte $00,$20,$20,$40,$60,$80,$A0,$C0
   .byte $D0,$B0,$90,$70,$50,$30,$30,$20
.endif

CODE_SEGMENT

;genTables:
   ldy   #$00
@colloop:
   tya
.if NTSC
   and   #$f0
   sta   $f0 ; TODO: rename
.else
   lsr
   lsr
   lsr
   lsr
   tax
.endif
   tya
   and   #$0f
   cmp   #$08
   bcc   :+
   eor   #$0f
:
   asl
.if NTSC
   ora   $f0; TODO: rename
.else
   ora   highs,x
.endif
   sta   colw,y

   iny
   bne   @colloop

; sine table

   ldx   #$3f

; Accumulate the delta (normal 16-bit addition)
@sinloop:
   clc
   lda   delta+0
   adc   value+0
   sta   value+0
   lda   delta+1
   adc   value+1
   sta   value+1

; Reflect the value around for a sine wave
   sta   sinew+$c0,y
   sta   sinew+$80,x
   eor   #$7f
   sta   sinew+$40,y
   sta   sinew+$00,x

; Increase the delta, which creates the "acceleration" for a parabola
   lda   #$08
   adc   delta+0   ; this value adds up to the proper amplitude
   sta   delta+0
   bcc   :+
   inc   delta+1
:

; Loop
   iny
   dex
   bpl   @sinloop

   ldy   #$00

:
   lda   siner,y
   sec
   sbc   siner,x
   asl
   asl
   asl
   asl
   sta   movpw,y
   eor   #$f0
   ;clc               ; obsolete since an additional "1" does not matter
   adc   #$10
   sta   movmw,y
   inx
   iny
   bne   :-

;   rts

