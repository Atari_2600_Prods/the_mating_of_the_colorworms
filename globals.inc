
; configuration parameters
.ifndef NTSC
.define NTSC 0
.endif

; timer values for vblank and overscan are for TIM64TI, screen is for TIM1KTI
; PAL values, determined by trial an error
; values are calculated by 2*timer value | timer index (0: TIM1KTI, 1: TIM64TI)
.if NTSC
.define TIMER_VBLANK   1+2*$23 ; ~2688 cycles
.define TIMER_SCREEN   0+2*$10
.define TIMER_OVERSCAN 1+2*$12 ; ~1280 cycles
.else
.define TIMER_VBLANK   1+2*$29 ; ~2688 cycles
.define TIMER_SCREEN   0+2*$13
.define TIMER_OVERSCAN 1+2*$16 ; ~1280 cycles
.endif

; all symbols visible to other source files

colr  := $f000
siner := $f100
movpr := $f200
movmr := $f300

colw  := colr  | $400
sinew := siner | $400
movpw := movpr | $400
movmw := movmr | $400

; sintable.s
.global   genTables

; payload.s
.global   payload
.globalzp revcnt

; song.s
.global   song

; main.s
.global   reset
.global   waitvblank   ; called with jsr; x,y untouched
.global   waitscreen   ; called with jsr; x,y untouched
.global   waitoverscan ; called with jmp
; alternative versions, that take use A instead of default for delay
.global   waitvblanka
.global   waitscreena
.global   waitoverscana
