
.include "vcs.inc"
.include "globals.inc"

.ifndef ARSC
.define ARSC 0
.endif

.ifndef SLOWMUSIC
.define SLOWMUSIC 0
.endif

.if ARSC
.define RODATA_SEGMENT .segment "RODATA3"
.define CODE_SEGMENT   .segment "CODE3"
.else
.define RODATA_SEGMENT .segment "RODATA"
.define CODE_SEGMENT   .segment "CODE"
.segment "VECTORS"
; nmi removed, because it will never happen on an 6507
.addr reset ; RESET
.addr waitoverscan + 8 ; IRQ: will only occur with brk and crash
.endif

.segment "ZEROPAGE"

RODATA_SEGMENT

song:
.include "song.inc"

CODE_SEGMENT

waitvblank:
   lda   #TIMER_SCREEN
   .byte $2c
waitscreen:
   lda   #TIMER_OVERSCAN
waitvblanka:
waitscreena:
@waitloop:
   bit   TIMINT
   bpl   @waitloop
   lsr
   sta   WSYNC
   sta   TIM64TI
   bcs   @no1k
   sta   TIM1KTI
@no1k:
   rol
   asl
   sta   VBLANK
   rts

reset:
   cld
   lda   #$00
:
   tsx
   pha
   bne   :-          ; SP=$FF, X = A = 0

.if ARSC
.include "gentables_arsc.inc"
.else
.include "gentables.inc"
.endif

.include "payload.inc"

