
.segment "ZEROPAGE"

fxcnt:
   .byte $00
frmcnt:
   .byte $00
revcnt:
   .byte $00
spridx:
   .byte $00
colptr:
   .word $0000
sndidx:
   .byte $00

RODATA_SEGMENT

; effects
; #: psa msa nsn grp col

; 0: $08 $1f $18 $c0 ; 1 thin with missles
; 1: $00 $00 $07 $1e ; snakes medium
; 2: $01 $01 $3a $ff ; 2 with missles
; 3: $0a $00 $05 $49 ; 3 thin
; 4: $00 $01 $3b $ff ; 3 with missles
; 5: $03 $03 $2e $f0 ; wamma
; 6: $03 $00 $07 $ff ; snakes fat

; col: >song >song^1 >movmr >siner >colr (movpr $00 $02)

pstartadd:
   .byte $08,$03,$03,$00,$0a,$01,$0b
mstartadd:
   .byte $1f,$00,$03,$01,$00,$01,$00
nusizenam: ; enam = nusiz >> 2
   .byte $18,$07,$3e,$3b,$05,$3a,$07
grpdata:
   .byte $c0,$ff,$ff,$ff,$49,$ff,$f0
coltab:
   .byte >movmr,>colr,>siner,>song,$00,(>song)^1,$02

CODE_SEGMENT

waitoverscan:
   lda   #TIMER_VBLANK
waitoverscana:
   jsr   waitscreena

   lda   #%00001110  ; each '1' bits generate a VSYNC ON line (bits 1..3)
@syncloop:
   sta   WSYNC
   sta   VSYNC       ; 1st '0' bit resets VSYNC, 2nd '0' bit exit loop
   lsr
   bne   @syncloop   ; branch until VSYNC has been reset

   inc   frmcnt
   bne   @skip
   lda   #$08
   and   SWCHB
   beq   @skip
.if 1
   ; 9 bytes counting backwards
   ldx   fxcnt
   dex
   bpl   :+
   ldx   #(mstartadd-pstartadd-1)
:
   stx   fxcnt
.else
   ; 11 bytes
   ldx   fxcnt
   inx
   cpx   #(mstartadd-pstartadd)
   bcc   :+
   ldx   #$00
:
   stx   fxcnt
.endif
@skip:
   dec   revcnt

   ldx   #$00
   ldy   frmcnt
   jsr   sprp
   ldy   revcnt
   jsr   sprp
   ldy   frmcnt
   jsr   sprm
   ldy   revcnt
   jsr   sprm

   ldy   fxcnt
   lda   nusizenam,y
   sta   NUSIZ0
   sta   NUSIZ1
   lsr
   lsr
   sta   ENAM0
   sta   ENAM1

   lda   grpdata,y
   sta   GRP0
   sta   GRP1

   lda   coltab,y
   sta   colptr+1

   sta   HMCLR

   jsr   waitvblank

   ldx   revcnt
   lda   colr,x
   ldy   #$00
   sta   WSYNC

@lineloop:
   sta   HMOVE
   sta   COLUP1

   ldx   frmcnt
   lda   colr,x
   sta   COLUP0

   lda   (colptr),y
   sta   COLUBK

   lda   movpr,x
   sta   HMP1
   lda   movmr,x
   sta   HMM1
   inc   frmcnt

   ldx   revcnt
   lda   movpr,x
   sta   HMP0
   lda   movmr,x
   sta   HMM0
   inc   revcnt

   lda   colr,x
   dey
.if NTSC
   cpy   #$2a
.else
   nop
.endif
   bne   @lineloop
.if 1
   sta   HMOVE
.endif

   jsr   waitscreen

.if NTSC
:
   inc   frmcnt
   inc   revcnt
   dey
   bne   :-
   sty   COLUBK
.endif

; beat
   lda   frmcnt
.if SLOWMUSIC
   lsr
.endif
   and   #$3c
   cmp   #$28
   beq   @snd
   and   #$0f
   bne   @nosnd
@snd:
   lda   #$0f
   and   revcnt
   .byte $2c
@nosnd:
   lda   #$00
   sta   AUDV1
   lda   #$07  ; $03
   sta   AUDC1
   lda   #$1c
   sta   AUDF1

; melody
   lda   revcnt
.if SLOWMUSIC
   and   #$0f
.else
   and   #$07
.endif
   tay
   bne   :+
   inc   sndidx
:
   lda   sndidx
   and   #$3f
   sta   sndidx
   tax
   lda   song,x
   tax
.if 0
   bpl   :+
   ldx   #$00
:
.endif
   alr   #$20
   lsr
   ora   #$04
   sta   AUDC0
   stx   AUDF0
   sty   AUDV0

   jmp   waitoverscan

sprm:
   sec
   lda   #$80
   sbc   siner,y
   ldy   fxcnt
   ;clc               ; c is always set
   adc   mstartadd,y
   bcc   spritepos    ; c is always clear

sprp:
   lda   siner,y
   ldy   fxcnt
   ;clc
   adc   pstartadd,y
spritepos:
; x = sprite to set
; a = position
;sta $f0,x
   sta   WSYNC
   sta   HMCLR
   clc

@loop:
   sbc   #$0f
   bcs   @loop
   eor   #$07
   asl
   asl
   asl
   asl
   sta   RESP0,x
   sta   HMP0,x
   sta   WSYNC
   sta   HMOVE
   inx
   rts

