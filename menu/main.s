
.include "vcs.inc"
.include "globals.inc"

.segment "ZEROPAGE"

.segment "RODATA"

demopartslo:
   .lobytes clean-1, splash-1, payload-1
demopartshi:
   .hibytes clean-1, splash-1, payload-1

.segment "CODE"

reset:
   cld
   ldx #$00
   txa
@clrloop:
   dex
   txs
   pha
   bne @clrloop ; SP=$FF, X = A = 0
   sta TIM1TI

waitoverscan:
   lda #TIMER_VBLANK
waitoverscana:
   jsr waitscreena
   
   ldx schedule
   php
   pla
   and #%00000100 ; irq flag
   bne @noclean
   tax
@noclean:

   lda #%00001110  ; each '1' bits generate a VSYNC ON line (bits 1..3)
@syncloop:
   sta WSYNC
   sta VSYNC       ; 1st '0' bit resets VSYNC, 2nd '0' bit exit loop
   lsr
   bne @syncloop   ; branch until VSYNC has been reset

   lda demopartshi,x
   pha
   lda demopartslo,x
   pha
   rts

waitvblank:
   lda #TIMER_SCREEN
   .byte $2c
waitscreen:
   lda #TIMER_OVERSCAN
waitvblanka:
waitscreena:
@waitloop:
   bit TIMINT
   bpl @waitloop
   lsr
   sta WSYNC
   sta TIM64TI
   bcs @no1k
   sta TIM1KTI
@no1k:
   rol
   asl
   sta VBLANK
   rts

clean:
   sei
   inc schedule      ; and store it where it belongs
   ldy #$00
   ldx #$2a
@clrtia:
   sty $02,x
   dex
   bne @clrtia
   jsr waitvblank

   ldx #localramstart
@clrram:
   sty $00,x
   inx
   bne @clrram
   jsr waitscreen
   bne waitoverscan
